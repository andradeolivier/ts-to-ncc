

# node-typescript-ncc-boilerplate

Boilerplate for creating fully commonjs binaries from typescript application

## Quick start

Edit file src/main.ts and run : npm start
And then a 'bin' file will be generated. 

## Available scripts

+ `start` - start compiled js file