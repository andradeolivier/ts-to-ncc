import * as fs from 'fs';
import async from 'async';
import { snakeCase } from 'change-case';

console.log(process.argv[2]);

async.mapSeries([1, 2 ,3], (numero: number, cb: Function) => {
  console.log(numero);
  const str = fs.readFileSync('./src/sample.txt').toString();
  cb(null, snakeCase(str));
}, (error: Error, result: any) => {
  if(error) {
    console.log(error);
  }
  console.log(result);
});
